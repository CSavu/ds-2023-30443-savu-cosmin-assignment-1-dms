package com.dms.rabbitmq.producers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DeviceUpdateProducer {
    @Value("${dms.rabbitmq.device.exchange}")
    private String deviceExchange;
    @Value("${dms.rabbitmq.device.delete.routing_key}")
    private String deviceDeleteRoutingKey;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void publishDeviceDeletionMessage(String deviceId) {
        rabbitTemplate.convertAndSend(deviceExchange, deviceDeleteRoutingKey, deviceId);
        log.info("Device deletion message sent for deviceId={}", deviceId);
    }
}
