package com.dms.services;

import com.dms.adapters.UserManagementServiceAdapter;
import com.dms.entities.Device;
import com.dms.entities.UserDeviceMapping;
import com.dms.exceptions.InvalidPropertyException;
import com.dms.rabbitmq.producers.DeviceUpdateProducer;
import com.dms.repositories.DeviceRepository;
import com.dms.repositories.UserDeviceMappingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DeviceService {
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private UserDeviceMappingRepository userDeviceMappingRepository;

    @Autowired
    private UserManagementServiceAdapter umsAdapter;

    @Autowired
    private DeviceUpdateProducer deviceUpdateProducer;

    public List<Device> getAllDevices() {
        return deviceRepository.findAll();
    }

    public Device createDevice(Device device) {
        if (deviceRepository.existsByNameAndAddress(device.getName(), device.getAddress()))
            throw new InvalidPropertyException();

        return deviceRepository.save(new Device(device.getName(), device.getAddress()));
    }

    @Transactional
    public boolean deleteDevice(Integer deviceId) {
        if (!deviceRepository.existsById(deviceId)) throw new InvalidPropertyException();

        try {
            deviceRepository.deleteById(deviceId);
            log.info("Deleted device with deviceId={}", deviceId);
            userDeviceMappingRepository.deleteAllByDeviceId(deviceId);
            log.info("Deleted all user-device mappings for deviceId={}", deviceId);
            deviceUpdateProducer.publishDeviceDeletionMessage(Integer.toString(deviceId));
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public Device updateDevice(Device device) {
        // maybe refactor and simplify this -> check device id, then if name+address combination doesn't exist, make the update
        // no need to allow a successful update if the name and address combination of the updated object is the same as before
        if (deviceRepository.existsById(device.getId())
                && deviceRepository.findAllByNameAndAddress(device.getName(), device.getAddress()).stream().allMatch(device1 -> Objects.equals(device1.getId(), device.getId()))) {
            Device oldDevice = deviceRepository.findById(device.getId()).orElseThrow(InvalidPropertyException::new);
            return deviceRepository.save(new Device(oldDevice.getId(), device.getName(), device.getAddress()));
        }
        throw new InvalidPropertyException();
    }

    public List<Device> getAllDevicesForUser(Integer userId) {
        List<UserDeviceMapping> userDeviceMappings = userDeviceMappingRepository.findAllByUserId(userId);

        return deviceRepository.findAllById(
                userDeviceMappings
                        .stream()
                        .map(UserDeviceMapping::getDeviceId).collect(Collectors.toList()));
    }

    public boolean createUserDeviceMapping(Integer deviceId, Integer userId, String authToken) {
        // check if user and device exist
        if (!umsAdapter.doesUserExist(userId, authToken) || !deviceRepository.existsById(deviceId))
            throw new InvalidPropertyException();

        // a device can only be assigned once
        if (userDeviceMappingRepository.findByDeviceId(deviceId).isPresent())
            throw new InvalidPropertyException();

        userDeviceMappingRepository.save(new UserDeviceMapping(userId, deviceId));
        return true;
    }

    @Transactional
    public boolean deleteAllUserDeviceMappings(Integer userId) {
        if (userDeviceMappingRepository.existsByUserId(userId)) {
            try {
                userDeviceMappingRepository.deleteAllByUserId(userId);
                log.info("Deleted all user device mappings for user with userId={}", userId);
                return true;
            } catch (Exception e) {
                log.error(e.getMessage());
                return false;
            }
        }
        return false;
    }

}
