package com.dms.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_generator")
    @SequenceGenerator(name="device_generator", sequenceName = "device_seq", allocationSize=1)
    private Integer id;

    @NonNull
    private String name;

    @NonNull
    private String address;
}
