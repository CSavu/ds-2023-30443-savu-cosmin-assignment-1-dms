package com.dms.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "user_device_mappings", uniqueConstraints = {
        @UniqueConstraint(columnNames = "deviceId")
})
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
public class UserDeviceMapping {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_device_generator")
    @SequenceGenerator(name = "user_device_generator", sequenceName = "user_device_seq", allocationSize = 1)
    private Integer id;

    @NonNull
    private Integer userId;
    @NonNull
    private Integer deviceId;
}
