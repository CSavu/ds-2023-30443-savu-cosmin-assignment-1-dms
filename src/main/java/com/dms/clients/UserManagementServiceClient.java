package com.dms.clients;

import com.dms.dtos.AuthorizationResponseDto;
import com.dms.dtos.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class UserManagementServiceClient {
    @Value("${dms.ums.client.authorization.verifyToken.url}")
    private String VERIFY_TOKEN_URL;
    @Value("${dms.ums.client.user.getById.url}")
    private String GET_USER_BY_ID_URL;

    private final RestTemplate restTemplate;

    @Autowired
    public UserManagementServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AuthorizationResponseDto verifyToken(String token) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);

            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

            ResponseEntity<AuthorizationResponseDto> responseEntity = restTemplate.postForEntity(
                    VERIFY_TOKEN_URL,
                    requestEntity,
                    AuthorizationResponseDto.class
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            // Handle general exceptions
            e.printStackTrace();
            return null;
        }
    }

    public UserDto getById(Integer userId, String token) {
        try {
            String url = UriComponentsBuilder.fromUriString(GET_USER_BY_ID_URL)
                    .queryParam("userId", userId)
                    .toUriString();

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);

            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

            ResponseEntity<UserDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    requestEntity,
                    UserDto.class
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            // Handle general exceptions
            e.printStackTrace();
            return null;
        }
    }
}
